import { Injectable, OnInit } from "@angular/core";
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Platform } from 'ionic-angular';
import { ConstantValues} from '../constants/ConstantValues';

@Injectable()
export class StoreCountData implements OnInit{

    passCount:number=-1;
    failCount:number=-1;
    databaseObj: SQLiteObject;
    StoreCountTable: string;
    id=1;
    constructor(
        private platform: Platform,
        private sqlite: SQLite, 
        databaseObj: SQLiteObject){
            this.databaseObj=databaseObj;
            

    }
    ngOnInit(): void {
        let constants=new ConstantValues();
        this.StoreCountTable=constants.getStoreCountTableName();
    }

    createCountTable() {
        this.databaseObj.executeSql(`
        CREATE TABLE IF NOT EXISTS ${this.StoreCountTable}  (pid INTEGER PRIMARY KEY AUTOINCREMENT,pass INTEGER,fail INTEGER)
        `, [])
          .then(() => {
            this.initializeCountTable();
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
          
      }
      //enter starting pass fail count to display
      initializeCountTable(){
        this.databaseObj.executeSql(`
        SELECT * FROM ${this.StoreCountTable}
        `
          , [])
          .then((res) => {
            if (res.rows.length <= 0) {
              this.passCount=0;
              this.failCount=0;
              this.insertCount(this.passCount,this.failCount);
    
            }
            else{
              this.getValues();
            }
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
      }
    // insert pass fail count to table
      insertCount(passCount:number,failCount:number) {
    
        this.databaseObj.executeSql(`
          INSERT INTO ${this.StoreCountTable} (pass,fail) VALUES ('${passCount}','${failCount}')
        `, [])
          .then(() => {
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
      }
    
      getValues(){
        this.databaseObj.executeSql(`
        SELECT pid,pass,fail FROM ${this.StoreCountTable}
        `
          , [])
          .then((res) => {
            this.id=res.rows.item(0).pid;
            this.passCount=res.rows.item(0).pass;
            this.failCount=res.rows.item(0).fail;
    
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
      }
    
    
    
      updatePass(passCount:number) {
        this.databaseObj.executeSql(`
          UPDATE ${this.StoreCountTable}
          SET pass = '${passCount}'
          WHERE pid = ${this.id}
        `, [])
          .then(() => {
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
      }
      updateFail(failCount:number) {
        this.databaseObj.executeSql(`
          UPDATE ${this.StoreCountTable}
          SET fail = '${failCount}'
          WHERE pid = ${this.id}
        `, [])
          .then(() => {
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
      }
}