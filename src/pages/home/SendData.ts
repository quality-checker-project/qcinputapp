import { Injectable } from "@angular/core";

@Injectable()
export class SendData {
    private item:string;
    private time:string;
    private quality:string;

  constructor(
    item:string,time:string,quality:string
  ) {
      this.setItem(item);
      this.setTime(time);
      this.setQuality(quality);
      
   }
   getItem():string{
       return this.item;
   }
   setItem(item:string){
       this.item=item;
   }
   getTime():string{
    return this.time;
    }
    setTime(time:string){
        this.time=time;
    }
    getQuality():string{
    return this.quality;   
    }
    setQuality(quality:string){
        this.quality=quality;
    }
}