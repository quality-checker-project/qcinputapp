import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, ToastOptions } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Platform } from 'ionic-angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Network } from '@ionic-native/network';
import { SendData} from './SendData';
import { OfflineDb} from '../../offlineAcess/offlineDb';
import { ConstantValues } from '../../constants/ConstantValues';
import { StoreCountData} from '../../storeCountAcess/StoreCountData';

@Component({
  selector: 'page-home',  
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  passCount:number=-1;
  failCount:number=-1;
  id;
  item:string;
  isConnected=true;
  toastOption:ToastOptions;

  databaseObj: SQLiteObject;
  databaseName: string;

  offlineDataAcess:OfflineDb;
  countDataAcess:StoreCountData;

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    public http: HttpClient,
    private network:Network,
    private toast:ToastController
  ) {

    this.network.onConnect().subscribe(()=>{
      this.isConnected=true;
    });
    this.network.onDisconnect().subscribe(()=>{
      this.isConnected=false;
    });
    this.platform.ready().then(() => {
      this.createDB();
    }).catch(error => {
      console.log(error);
    });
    this.toastOption={
      message:"disconnected",
      duration:2000
    }
    
  }
  ngOnInit(): void {
    let constants=new ConstantValues();
    this.item=constants.getItem();
    this.databaseName=constants.getDataBaseName();

    
  }
  postQualityFail(){
    this.failCount++;
    this.countDataAcess.updateFail(this.failCount);
    let data= new SendData(this.item,this.getDateAndTime(),"fail");
    if(this.isConnected){
      this.offlineDataAcess.sendOffineData(data);
    }else{
      this.offlineDataAcess.insertOfflineData(data);
      this.toast.create(this.toastOption).present();
    }  
    
  }
  postQualityPass(){
    this.passCount++;
    this.countDataAcess.updatePass(this.passCount);
    let data= new SendData(this.item,this.getDateAndTime(),"pass");
    if(this.isConnected){
      this.offlineDataAcess.sendOffineData(data);
    }else{
      this.offlineDataAcess.insertOfflineData(data);
      this.toast.create(this.toastOption).present();
    } 
    
  }

  getDateAndTime():string{
    const dateObj=new Date();
    return Math.round(dateObj.getTime()).toString();
  }
  createDB() {
    this.sqlite.create({
      name: this.databaseName,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db;
        this.offlineDataAcess= new OfflineDb(this.platform,this.sqlite,this.http,db);
        this.countDataAcess= new StoreCountData(this.platform,this.sqlite,db);

        this.countDataAcess.createCountTable();
        this.offlineDataAcess.createOfflineTable();
        /*
        do {
          this.passCount=this.countDataAcess.passCount;
          this.failCount=this.countDataAcess.failCount;
       } while(this.passCount==-1 || this.failCount==-1)
       */
      })
      .catch(e => {
        console.log("error "+JSON.stringify(e));
      });
  }

}


