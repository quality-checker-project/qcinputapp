import { Injectable } from "@angular/core";
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class DataHeaders {
    headers;
    countDataHeader():HttpHeaders{
        let headers = new HttpHeaders();
        headers.append('Accept','application/json');
        headers.append('Content-Type','application/json');
        headers.append('Access-Control-Allow-Origin','http://43d8aa62.ngrok.io');
        return headers;
    }
}