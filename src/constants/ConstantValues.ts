export class ConstantValues{
    readonly databaseName: string = "inputData.db";
    readonly StoreCountTable: string = "count";
    readonly offlineTable: string="offline";
    item: string="denim";
    url:string="http://43d8aa62.ngrok.io/event/add";

    getDataBaseName():string{
        return this.databaseName;
    }
    getStoreCountTableName():string{
        return this.StoreCountTable;
    }
    getOfflineTableName():string{
        return this.offlineTable;
    }
    getItem():string{
        return this.item;
    }
    getUrl():string{
        return this.url;
    }

}