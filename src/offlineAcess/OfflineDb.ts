import { Injectable, OnInit } from "@angular/core";
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SendData} from '../pages/home/SendData';
import { Platform } from 'ionic-angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { DataHeaders} from '../headers/DataHeaders';
import { ConstantValues} from '../constants/ConstantValues';

@Injectable()
export class OfflineDb implements OnInit{
    databaseObj: SQLiteObject;
    offlineTable: string;
    url:string;
    headers;
    constructor(
        private platform: Platform,
        private sqlite: SQLite,
        public http: HttpClient, 
        databaseObj: SQLiteObject){
            this.databaseObj=databaseObj;
            

    }
    ngOnInit(): void {
        this.headers= (new DataHeaders()).countDataHeader();
        let constants=new ConstantValues();
        this.offlineTable=constants.getOfflineTableName();
        this.url=constants.getUrl();

    }
    createOfflineTable() {
        this.databaseObj.executeSql(`
        CREATE TABLE IF NOT EXISTS ${this.offlineTable}  (pid INTEGER PRIMARY KEY AUTOINCREMENT,item varchar(255),time varchar(255), quality varchar(255))
        `, [])
          .then(() => {
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
          
      }
      insertOfflineData(data:SendData) {
    
        this.databaseObj.executeSql(`
          INSERT INTO ${this.offlineTable} (item,time,quality) VALUES ('${data.getItem()}','${data.getTime()}','${data.getQuality()}')
        `, [])
          .then(() => {
          })
          .catch(e => {
            console.log("error "+JSON.stringify(e));
          });
      }
      sendOffineData(data:SendData){
        this.databaseObj.executeSql(`
        SELECT * FROM ${this.offlineTable}
        `
          , [])
          .then((res) => {
            if (res.rows.length > 0) {
              for (var i = 0; i < res.rows.length; i++) {
                let currentItem=res.rows.item(i).item;
                let currentTime=res.rows.item(i).time;
                let currentQuality=res.rows.item(i).quality;
                let currentData=new SendData(currentItem,currentTime,currentQuality);
                this.postReq(currentData);
              }
            }
            this.databaseObj.executeSql(`
            DELETE FROM ${this.offlineTable}`,[])
            .then((res)=>{
              this.postReq(data);
            });
          })
          .catch(e => {
            alert("error " + JSON.stringify(e))
          });
      }
      postReq(data:SendData){
        this.http.post(this.url,data,{headers:this.headers})
        .subscribe(response=>{
        },error=>{
          console.log("this error",error);
        }
        );
      }
}